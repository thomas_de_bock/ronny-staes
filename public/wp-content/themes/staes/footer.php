
	
		        <footer class="clearfix">
					<div class="clearfix">
						<div class="col-md-1"></div>
						<div class="clearfix col-md-10">
							<div class="col-md-2">
								<a class="footerlogo" href="index.html"><img alt="logo Footer"
								src="<?php bloginfo('template_directory'); ?>/img/footer-logo.svg"></a>
							</div>
							<div class="col-md-4">
								<p>Bepleisteringen Ronny Staes</p>
								<p>Pareinpark 79</p>
								<p>B-9120 Beveren-Waas</p>
								<br>
								<p class="emp">© Staes ronny bvba - algemene voorwaarden</p>
							</div>
							<div class="col-md-4">
								<p><a href="tel:+3232524317">T +32 3 252 43 17</a></p>
								<p>F +32 3 252 54 63</p>
								<p><a href="tel:+32475378403">G +32 4755 37 84 03</a></p>
							</div>
							<div class="col-md-2">
								<p><a href="mailto:ronny@staesronny.be">ronny@staesronny.be</a></p>
								<p>BTW BE 0462.122.450</p>
							</div>
						</div>
						<div class="col-md-1"></div>
					</div>
				</footer>
	       </section>
	       </div>
       </div>
       <?php wp_footer(); ?>
    </body>
</html>