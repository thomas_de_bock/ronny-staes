<?php get_header(); ?>
	        <div id="container">
	        <section id="main" class="">
	        <?php while ( have_posts() ) : the_post(); ?>
		        <section id="content">
			       <div class="clearfix">
				       <div class="image-wrapper image-home col-lg-6 col-no-gutter fill-image" data-mh="intro">
					       <img src="<?php the_field('Frontpage_image'); ?>" alt="" />
				       </div>
					   <article class="col-lg-6 col-no-gutter left-red-border intro-content" data-mh="intro">
						   <div class="vertical-center">
							   <p><?php the_field('frontpage_tekst'); ?></p>
						   </div>
					   </article>
			       </div>
			       <div class="werkwijze-slider clearfix">
				       <ul class="col-md-4 col-no-gutter werkwijze-text">
				       
				       <?php
						// check if the repeater field has rows of data
						if( have_rows('frontpage_slides') ):
							// loop through the rows of data
							$count= count(get_field('frontpage_slides'));
							$id= 1;
							while ( have_rows('frontpage_slides') ) : the_row(); 
						?>
								
							<li class="slide" data-mh="intro-slider">
						       <article class="slide-article" data-mh="intro-slider">
								    <div class="article--header clearfix">
									    <h3>Werkwijze<span class="counter"><?php echo $id; ?>/<?php echo $count; ?></span></h3>
								    </div>
								    <div class="article--body">
								    <h4><?php the_sub_field('slide_titel'); ?></h4>
								    <p><?php the_sub_field('slide_beschrijving'); ?></p>
								   </div>
								   	<div class="article--footer clearfix">
									    <a href="#" class="button button-alternate">Vorige stap</a>
									    <a href="#" class="button button-alternate button-right">Volgende stap</a>
								    </div>
							    </article>
						    </li>
						<?php 
							$id++;
							endwhile;
						endif;
						?>
						</ul>
						<ul class="col-md-8 col-no-gutter werkwijze-afbeelding">
						<?php
						// check if the repeater field has rows of data
						if( have_rows('frontpage_slides') ):
							// loop through the rows of data
							while ( have_rows('frontpage_slides') ) : the_row(); 
						?>
						   <li class="slide">
							    <figure class="fill-image" data-mh="intro-slider">
							       	<img src="<?php the_sub_field('slide_image'); ?>" alt="">
							    </figure>
						   </li>
						<?php 
							endwhile;
						endif;
						?>
						</ul>
			       </div>
			       <div class="left-red-border isolatie-article">
				       <div class="article--header clearfix">
							<h3>Isolatie en premies</h3>
						</div>
						<div class="article--body">
							<p>Wanneer u kiest voor het plaatsen van isolatie, kan u in aanmerking komen voor een premie. Gelieve u hierover goed te informeren op volgende instanties:</p>
						</div>
						
						<div class="article--footer clearfix">
						<?php if( have_rows('frontpage_links') ): 
								while ( have_rows('frontpage_links') ) : the_row();
						?>
							<a href="<?php the_sub_field('link_url'); ?>" class="button col-md-3"><?php the_sub_field('link_naam'); ?></a>
						<?php
							endwhile;	
						endif; 
						?>
						</div>
			       </div>
		        </section>
				<?php endwhile; ?>


<?php get_footer(); ?>
