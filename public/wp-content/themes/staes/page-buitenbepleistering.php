<?php get_header(); ?>


	        <div id="container">
	        <?php while ( have_posts() ) : the_post(); ?>
	        <section id="main" class="">
		       <section id="content">
			       <div class="left-red-border">
					    <dl class="accordion">
					    
					    <?php
						if( have_rows('buitenbepleistering_lijst') ):
							while ( have_rows('buitenbepleistering_lijst') ) : the_row(); 
						?>
							<dt><a href="#"><?php the_sub_field('titel'); ?></a></dt>
							<dd>
								<?php the_sub_field('beschrijving'); ?>
							</dd>
						
						<?php 
							endwhile;
						endif;
						?>
						</dl>
					</div>
		       </section>
	       <?php endwhile; ?>
	       
	       
<?php get_footer(); ?>
