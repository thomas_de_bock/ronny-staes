<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html <?php language_attributes(); ?> class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
		<meta charset="<?php bloginfo('charset'); ?>">
		<title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' :'; } ?> <?php bloginfo('name'); ?></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        
        <?php wp_head(); ?>

    </head>
    <body <?php body_class(); ?>>
	   	<div class="pagewrapper clearfix">
		   	<header class="clearfix">
			   	<button id="main-nav-toggle" class="main-nav-toggle menu-btn">
					<div class="main-nav-toggle-icon">
						<span class="main-nav-toggle-line main-nav-toggle-line--1"></span>
						<span class="main-nav-toggle-line main-nav-toggle-line--2"></span>
						<span class="main-nav-toggle-line main-nav-toggle-line--3"></span>
					</div>
				</button>
				<div class="logowrapper">
				    <a href="index.html">
					   <img src="<?php bloginfo('template_directory'); ?>/img/logo.svg" alt="Logo">
				    </a>
			    </div>
		   	</header>
		   	<div class="site-overlay"></div>
        
	        <aside id="sidebar" class="pushy pushy-left">
		        <div class="fixed-item">
			        <div class="logowrapper">
				       <a href="index.html">
					   		<img src="<?php bloginfo('template_directory'); ?>/img/logo.svg" alt="Logo">
				       </a>
			        </div>
			        <nav>
				       <ul>
							<?php wp_nav_menu(array('theme_location' => 'header-menu')) ?>
				       </ul>
			       </nav>
		       </div>
	        </aside>

