<?php get_header(); ?>

<div id="container">
	        <section id="main" class="">
		        <section id="content">
					<div class="clearfix map-content">
						<div class="col-md-6" data-mh="map">
							<div class="vertical-center contact-info">
								<p>
									<span class="emp">Bepleisteringen</span><br>
									<span class="emp">Ronny Staes BVBA</span>
								</p>
								<p>
									Pareinpark 79
								</p>
								<p>
									9120 Beveren-Waas
								</p>
								<br>
								<p>
									<a href="tel:+322524317">T +32 252 43 17</a>
								</p>
								<p>
									F +32 252 54 63
								</p>
								<p>
									<a href="tel:+32475378403">G +32 475 37 84 03</a>
								</p>
								<br>
								<p>
									<a href="mailto:info@staesronny.be">info@staesronny.be</a>
								</p>
								<p>
									BTW BE 0462 122 450
								</p>
							</div>
						</div>
						<div class="col-md-6" id="map-canvas" data-mh="map"></div>
					</div>
					<div class="clearfix left-red-border formulier-wrapper">
						<div class="article--header clearfix">
							<h3>Contacteer ons</h3>
						</div>
						<div class="article--body">
						
						<?php gravity_form( 1, false, false, false, '', false ); ?>
						
						</div>
					</div>
		        </section>

<?php get_footer(); ?>
