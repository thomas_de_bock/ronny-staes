<?php get_header(); ?>
<?php
	$the_query = new WP_Query( "post_type=projecten" ); 
?>
<div id="container">
	        <section id="main" class="">
	        <?php while ( have_posts() ) : the_post(); ?>
		       <section id="content">
			       <div class="left-red-border">
				       <div class="project--detail clearfix">
					       <article class="col-md-4 col-no-gutter" data-mh="project-detail">
						        <div class="article--header clearfix">
							       <h3><?php the_field('titel'); ?></h3>
						       	</div>
						       	<div class="article--body">
							    <?php the_field('beschrijving'); ?>
						       	</div>
						       	<div class="article--footer clearfix">
							       	<a href="#" class="button button-black">Vorige foto</a>
							       	<a href="#" class="button button-black button-right">Volgende foto</a>
						       	</div>
					       </article>
					       <div class="foto-slider col-md-8">
						       <ul>
					       	  
					       	  <?php 
									$images = get_field('gallery');
									foreach( $images as $image ): 
								?>
						       	  <li class="slide" data-mh="project-detail">
						       		 <figure class="afbeelding fill-image">
							       		 <img src="<?php echo $image['url']; ?>" alt="">
						       		 </figure>
						       	  </li>
						       	  <?php endforeach; ?>
						       </ul>
					       </div>
				       </div>
			       </div>
			       <div class="left-red-border clearfix project-grid">
			       
			       <?php
						$alt = ''; 
						while ( $the_query->have_posts() ) : $the_query->the_post();
						if($alt == 'col-no-gutter')
							$alt='';
						else
							$alt = 'col-no-gutter';
							
					?>
			       
						<div class="col-md-6 <?php echo $alt; ?> project-item">
					       <a href="<?php the_permalink(); ?>">
						       <figure class="fill-image">
					       			<?php 
										$images = get_field('gallery'); 
										echo '<img src="'.$images[0]['url'].'" alt="">';
								  	?>
						       		
						       		<figcaption>
						       			<?php the_field('titel'); ?>
						       		</figcaption>
						       </figure>
					       </a>
				       </div>
				       
				       <?php endwhile; ?>
				      
					</div>
		       </section>
		       <?php endwhile; ?>

<?php get_footer(); ?>
