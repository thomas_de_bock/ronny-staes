<?php get_header(); ?>
<?php
	$the_query = new WP_Query( "post_type=projecten" ); 
?>
<div id="container">
	        <section id="main" class="">
		       <section id="content">
			       <div class="left-red-border clearfix project-grid">
			       <?php
						$alt = ''; 
						while ( $the_query->have_posts() ) : $the_query->the_post();
						if($alt == 'col-no-gutter')
							$alt='';
						else
							$alt = 'col-no-gutter';
							
					?>
			       	
				       <div class="col-md-6 <?php echo $alt; ?> project-item">
					       <a href="<?php the_permalink(); ?>">
						       <figure class="fill-image">
					       			<?php 
										$images = get_field('gallery'); 
										echo '<img src="'.$images[0]['url'].'" alt="">';
								  	?>
						       		
						       		<figcaption>
						       			<?php the_field('titel'); ?>
						       		</figcaption>
						       </figure>
					       </a>
				       </div>
				       <?php endwhile; ?>
					</div>
		       </section>
<div>
</div>
<?php get_footer(); ?>
