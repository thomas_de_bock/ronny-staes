<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link http://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'scotchbox');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'OF1|n#qj-ts(@a3dPE!RcjrcH{OKh{Xf6F+Uk^xZ3mXK0Uv?WcVCR#F =d%A5I-K');
define('SECURE_AUTH_KEY',  'zZb{sTpXvZ3uMV*2i,yD}<:}XcgWW-([`),|7rUL)9:9j}wZJ|-;U$FJ4D|oyJ(B');
define('LOGGED_IN_KEY',    'UKMQXm(~HCw~dK8T+FvHbf@2ML` }{@-|Bb9y( 21W-;GNDBTNS96%>h%N-?J+Y:');
define('NONCE_KEY',        '/yc+CvYAqwE[h:uG>vzK(^3:/GfGh|Yj2|&@yS,Bqcl&}#-}dv&tK+M_[dtfq|/T');
define('AUTH_SALT',        '}=sX+b%CRmG+{N>$ZWZc_Kh*#{rNjaHw_9<1ZW.y_cOt#X%6u=KZ2od.Nm_z2`R/');
define('SECURE_AUTH_SALT', '1hTgoOgK&~td]|Sk6t N8dCt2=lWI#MekAVu4FM|=qL-TLRbkQG:}I/cJOExh#ga');
define('LOGGED_IN_SALT',   ';_LoCTjJy>;rEc/r1V/fg-tc}Zd/!+hID}RPu5cd7eh*At<l8-S=8;mc(UR 4`-&');
define('NONCE_SALT',       'XKEUF*44o!zfXC%QQrD2W[/pAHpm@;!~RsZUY$*P]hO|v-oU.)JAVN|eeK+SNU?~');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
