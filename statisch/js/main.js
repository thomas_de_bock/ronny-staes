jQuery("document").ready(function($){

	function openFirstPanel(){
	  	$('.accordion > dt:first-child').addClass('active').next().addClass('active').slideDown();
	}
	var map;
	function makeMaps(){
		var mapOptions = {
            center: new google.maps.LatLng(51.220835,4.265643),
            zoom: 14,
            zoomControl: false,
            disableDoubleClickZoom: false,
            mapTypeControl: false,
            scaleControl: true,
            scrollwheel: false,
            panControl: false,
            streetViewControl: false,
            draggable : true,
            overviewMapControl: false,
            overviewMapControlOptions: {
                opened: false,
            },
            mapTypeId: google.maps.MapTypeId.ROADMAP,
        }
        var mapElement = document.getElementById('map-canvas');
        var map = new google.maps.Map(mapElement, mapOptions);

        var marker = new google.maps.Marker({
	        position: new google.maps.LatLng(51.220835,4.265643),
	        map: map
        });

        var infowindow = new google.maps.InfoWindow({
	      	content: "<div><p><strong>Ronny Staes</strong></p><p>Pareinpark 79</p><p>9120 Beveren-Waas</p></div>"
	  	});

	  	google.maps.event.addListener(marker, 'click', function() {
		    infowindow.open(map,marker);
		});
	}

	$.fn.matchHeight._update();

	var werkWijzeSliderText = $(".werkwijze-text").slick({
		"slide": ".slide",
		"arrows": false,
		'asNavFor': '.werkwijze-afbeelding',
		'fade': true,
		'adaptiveHeight': true,
		'responsive': [
	    {
	      breakpoint: 1040,
	      settings: {
	        'adaptiveHeight': false
	      }
    	}]
	});
	var werkWijzeSliderAfbeelding = $(".werkwijze-afbeelding").slick({
		"slide": ".slide",
		"arrows": false,
		'asNavFor': '.werkwijze-text'
	});

	werkWijzeSliderText.on("init", function(){
		$.fn.matchHeight._update();
		werkWijzeSliderText.slick("slickNext");
	});

	$(".werkwijze-slider .slide-article").on("click", ".button", function(e){
		if($(this).hasClass("button-right")){
			werkWijzeSliderText.slick("slickNext");
		} else {
			werkWijzeSliderText.slick("slickPrev");
		}
		e.preventDefault();
	});

	var projectDetailSlider = $(".foto-slider ul").slick({
		"slide": ".slide",
		"arrows": false
	});
	projectDetailSlider.on("init", function(){
		$.fn.matchHeight._update();
	});

	$(".project--detail").on("click", ".button", function(e){
		if($(this).hasClass("button-right")){
			projectDetailSlider.slick("slickNext");
		} else {
			projectDetailSlider.slick("slickPrev");
		}
		e.preventDefault();
	});

	$(".fill-image").imgLiquid();

	var allPanels = $('.accordion > dd').hide();

	openFirstPanel();

    $('.accordion > dt > a').click(function(e) {
	      $this = $(this);
	      $target =  $this.parent().next();

	      if(!$target.hasClass('active')){
	         allPanels.removeClass('active').slideUp();
	         $target.addClass('active').slideDown();


	         $(this).parent('dt').siblings('dt').removeClass("active");
	         $(this).parent('dt').addClass("active");
	      } else {
		      allPanels.removeClass('active').slideUp();
		      $(this).parent('dt').removeClass("active");
	      }

		  e.preventDefault();
	});

	if($("#map-canvas").length){
		makeMaps();
	}

	$(".fixed-item").sticky();

	if (matchMedia('only screen and (max-width: 1040px)').matches) {
		$('*[data-mh]').matchHeight('remove');
		$(".project--detail article").before($(".foto-slider"));
		$(".werkwijze-text").before($(".werkwijze-afbeelding"));
	} else {
		$(".foto-slider").before($(".project--detail article"));
		$(".werkwijze-afbeelding").before($(".werkwijze-text"));
	}
	if (matchMedia('only screen and (min-width: 768px) and (max-width: 1040px)').matches) {
		$(".map-content").height($(".map-content").height());
	}

});
